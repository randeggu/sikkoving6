#include <string.h>
#include <stdlib.h>
#include <stdint.h>


char *task2(const char *str, int size){

    if(!str || !size){
        return NULL;
    }

    char *out_string = (char *) malloc(size *  6 *sizeof(char));

    for(int i = 0, j = 0; i < size; i++, j++) {
        if(str[i] == 10 || str[i] == 0){
            out_string[j] = 0;
            break;
        }


        switch(str[i]) {
            case '&':
                strncpy(&out_string[j], "&amp;", 5);
                j += 4;
                break;
            case '<':
                strncpy(&out_string[j], "&lt;", 4);
                j += 3;
                break;
            case '>':
                strncpy(&out_string[j], "&gt;", 4);
                j += 3;
                break;
            default:
                out_string[j] = str[i];
                break;
        }
    }
    return out_string;
}



int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {

    if(Size < 1 || !Data){
        return 0;
    }

    const char *cstring = (const char*) Data;

    // Checks if the string is terminated.
    if(cstring[Size -1] != 0 && cstring[Size -1] != 10){
        return 0;
    }


    char *res = task2(Data, Size);


    free(res);

    return 0;
}